﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NewBehaviourScript : MonoBehaviour {
    public Button btn;
    public InputField p_field;
    public InputField initial_fund_field;
    public InputField amount_field;
    public InputField num_round_field;
    public InputField speed_field;
    public Text result_field;
    private IEnumerator coroutine;
    private bool running = false;
    // Use this for initialization
    void Start() {
        btn.onClick.AddListener(StartSimulation);
    }

    // Update is called once per frame
    void Update() {
      
    }

    void StartSimulation()
    {
        float p,speed;
        int fund, amount,num_round;
        float.TryParse(p_field.text,out p);
        int.TryParse(initial_fund_field.text,out fund);
        int.TryParse(amount_field.text,out amount);
        int.TryParse(num_round_field.text, out num_round);
        float.TryParse(speed_field.text, out speed);
        result_field.text = fund.ToString();
        StartCoroutine(NextRound(p,fund,amount,num_round,speed));
    }

    private IEnumerator NextRound(float p,int fund,int amount,int num_round,float speed)
    {
        int currentBet = amount;
        for (int i = 1; i <= num_round &&  fund>0; i++)
        {
            float roll = Random.Range(0.0f, 1.0f);
            if (roll > p) //player lose this round
            {
                fund -= currentBet;
                currentBet = currentBet*2;
            }
            else
            {
                fund += currentBet;
                currentBet = amount;
            }
            print("fund"+fund);
            result_field.text = fund.ToString();
            yield return new WaitForSeconds(speed);
        }
        print("Done ");
    }
}
